# Nichoire hirondelle avec moule 3D

<img src=https://framagit.org/LPO_CVDL/nichoire-hirondelle-avec-moule-3d/-/raw/main/Photos/Hirondelle_de_fenetre_MICHEL_HALIN.jpg alt="texte alternatif" width=30% height=10% >

# 1.	Avant-propos

Les hirondelles sont protégées par la loi. L’enlèvement, la destruction des nids, couvées, poussins et adultes sont interdits et constituent un délit (150 000 € d’amende et 3 ans de prison). Pour tout renseignement sur la cohabitation avec ces oiseaux, contactez votre association naturaliste locale.
Les nichoirs artificiels n’ont pas vocation à remplacer ceux naturels fabriqués par les hirondelles mais viennent en complément.
Pour qu’un nichoir à hirondelle soit efficace, il faut qu’il soit installé dans un milieu convenant à l’espèce avec des insectes et de l’eau à proximité. Les hirondelles de fenêtre s’installeront sur les façades des habitations, tandis que les hirondelles rustiques préfèreront les caves, granges ou vieux bâtiments.


## 2.	Qui sommes-nous ?

Association naturaliste loi 1901 affiliée à la LPO France, la LPO Centre-Val de Loire a pour mission la sauvegarde des oiseaux, des chauves-souris et des milieux naturels les accueillant la région Centre-Val de Loire. Organisme reconnu d’intérêt général, elle œuvre au quotidien pour enrichir les connaissances sur la biodiversité, protéger les espèces et leurs milieux et sensibiliser tous les publics. 
Pour mener à bien ses missions, l’association peut compter sur le soutien de près de 3000 adhérents et 260 bénévoles sur toute la région. Une équipe permanente de 11 salariés travaille tout au long de l’année et est appuyée par des volontaires en service civique et stagiaires qui évoluent à ses côtés. 
*Depuis de nombreuses années, la LPO en région Centre-Val de Loire met en place des actions concrètes de préservation de la biodiversité locale : réalisation d’inventaires naturalistes, préservation des espèces et de leurs milieux, construction et pose d’abris pour la faune sauvage, suivis d’espèces protégées… Elle intervient auprès des publics de tous âges afin de leur faire découvrir la richesse de notre patrimoine naturel.

Pour tout renseignement complémentaire sur ce projet, contactez XXXXX@XXX.fr 


## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
